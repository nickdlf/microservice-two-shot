# shoes_rest/api_urls.py

from django.urls import path
from .views import api_list_shoes, api_show_shoes

app_name = "shoes_rest"

urlpatterns = [
    # path("bin/<int:bin_vo_id>/shoes/", api_list_shoes, name="api_list_shoes"),
    path("shoes/<int:pk>/", api_show_shoes, name="api_show_shoes"),
    path("shoes/", api_list_shoes, name="api_list_shoes"),
]
