from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import BinVO, Shoe


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
                    "import_href",
                    "closet_name",
                    "bin_number",
                    "bin_size",
                ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
                  "id",
                  "model_name",
                  "color",
                  "manufacturer",
                  "picture_url",
                ]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
                  "id",
                  "model_name",
                  "color",
                  "manufacturer",
                  "picture_url",
                ]
    encoders = {
        "bin": ShoeListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_shoes(request, pk):

    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)

        return JsonResponse(
            {"shoe": shoe},
            encoder=ShoeDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    else:

        content = json.loads(request.body)
        try:

            if "bin" in content:

                bin = BinVO.objects.get(id=content["bin"])
                content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin_vo id"},
                status=400,
            )

        Shoe.objects.filter(id=pk).update(**content)
        conference = Shoe.objects.get(id=pk)
        return JsonResponse(
            conference,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
