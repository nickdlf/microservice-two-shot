from django.db import models


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200, null=True)
    bin_number = models.CharField(max_length=100, null=True)  # Specify the max_length
    bin_size = models.CharField(max_length=100, null=True)  # Specify the max_length


class Shoe(models.Model):
    model_name = models.CharField(max_length=40, null=True)
    color = models.CharField(max_length=40, null=True)
    manufacturer = models.CharField(max_length=40, null=True)
    picture_url = models.CharField(max_length=200, null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )
