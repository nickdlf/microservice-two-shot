import React, { useEffect, useState } from 'react';

function ShoeList(props) {
    const [shoes, setShoes] = useState([]);

        async function deleteShoe(shoeId) {
            const response = await fetch(`http://localhost:8080/api/shoes/${shoeId}`, {
            method: 'DELETE',
            });

            if (response.ok) {
              props.getShoes();
            } else {
              console.error('Failed to delete shoes');
            }
        }


    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Picture Link</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes.map((shoe) => {
            return (
              <tr key={shoe.id}>
                {console.log(shoe.id)}
                {/* "why the devil", can't I use shoe.id for
                    my key value, but index works?  */}
                <td>{ shoe.model_name }</td>
                <td>{ shoe.picture_url }</td>
                <td>
                    <button onClick={() => deleteShoe(shoe.id)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default ShoeList;
