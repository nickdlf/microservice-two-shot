import React, { useState, useEffect } from 'react';

function ShoeForm({ getShoes, bins }) {
  const [model_name, setModelName] = useState('');
  const [color, setColor] = useState('');
  const [manufacturer, setManufacturer] = useState('');
  const [picture_url, setPictureURL] = useState('');
  const [bin, setBin] = useState('');

  async function handleSubmit(event) {
    event.preventDefault();

    const data = {
      model_name,
      color,
      manufacturer,
      picture_url,
      bin,
    };

    const shoeUrl = 'http://localhost:8080/api/shoes/';

    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const shoeResponse = await fetch(shoeUrl, fetchOptions);
    if (shoeResponse.ok) {
      setModelName('');
      setColor('')
      setManufacturer('');
      setPictureURL('');
      setBin('');
      getShoes();
    } else {
        console.error('An error occurred when creating the shoes')
    }
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new pair of shoes</h1>
          <form onSubmit={handleSubmit} id="create-shoes-form">
            <div className="form-floating mb-3">
              <input value={model_name} onChange={(e) => setModelName(e.target.value)} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" />
              <label htmlFor="model_name">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={color} onChange={(e) => setColor(e.target.value)} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Colors</label>
            </div>
            <div className="form-floating mb-3">
              <input value={manufacturer} onChange={(e) => setManufacturer(e.target.value)} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input value={picture_url} onChange={(e) => setPictureURL(e.target.value)} placeholder="Picture URL" required type="url" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="mb-3">
            <select value={bin} onChange={(e) => setBin(e.target.value)} required name="bin" id="bin" className="form-select">
                <option value="">Choose a bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.id} value={bin.href}>{bin.closet_name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;
