import { BrowserRouter, Routes, Route, } from 'react-router-dom';
import React, { useEffect, useState } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList';
import HatForm from './HatForm';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';

function App(props) {

  const [hats, setHats] = useState([]);
  const [locations, setLocations] = useState([])
  const [shoes, setShoes] = useState([]);
  const [bins, setBins] = useState([])


  async function getHats(){
    const response = await fetch("http://localhost:8090/api/hats/");
    if (response.ok){
      const { hats } = await response.json()
      setHats(hats)
    } else {
      console.error('An error occureed fetching the data')
    }
  }

  async function getLocations() {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  async function getShoes() {
      const response = await fetch("http://localhost:8080/api/shoes/");
      if (response.ok){
        const { shoes } = await response.json()
        setShoes(shoes)
      } else {
        console.error('An error occureed fetching the data')
      }
    }

  async function getBins() {
      const url = 'http://localhost:8100/api/bins/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setBins(data.bins);
      }
    }

  useEffect(() => {
    getHats();
    getLocations();
    getShoes();
    getBins();
  }, [])

  if (hats === undefined || shoes === undefined) {
    return null;
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="hats">
            <Route index element={<HatList hats={hats} />} />
            <Route path="new" element={<HatForm  locations={locations} getHats={getHats} />} />
            <Route path="edit/:hatId" element={<HatForm  locations={locations} getHats={getHats}/>} />
          </Route>

          <Route path="shoes">
            <Route index element={<ShoeList shoes={shoes} getShoes={getShoes}  />} />
            <Route path="new" element={<ShoeForm bins={bins} getShoes={getShoes} />} />
            
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
