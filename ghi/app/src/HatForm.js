import React, { useState, useEffect } from 'react';
import { useParams,} from 'react-router-dom';

function HatForm({hats, getHats, locations}) {
  const [color, setColor] = useState('');
  const [fabric, setFabric] = useState('');
  const [style, setStyle] = useState('');
  const [picture_url, setPictureURL] = useState('');
  const [location, setLocation] = useState('');
  const {hatId} = useParams();
  const [updateOrCreateString, setUpdateOrCreateString] = useState('Create');
  const [editFormBoolean, setEditFormBoolean] = useState(null)
  // const [{isEditForm}, setIsEditForm] = useState(true)
  // const [isEditForm, setIsEditForm] = useState(false)
    // console.log('Locations prop in HatForm:', locations);
  

    // 1. fetch the hat data that corresponds with hatId
            // useEffect for pre filled form
    // 2. pre fill form.....??
    // 3. update event handler
    
    
    
    useEffect(()=> {
      if (hatId){
        setEditFormBoolean(true)
        console.log('edit form boolean = ', editFormBoolean)
      } else {
        setEditFormBoolean(false)
      }
    }, [hatId]);

    useEffect(()=> {
      console.log('TOP EFB =', editFormBoolean)
      if (editFormBoolean){
        fetchHat();
        setUpdateOrCreateString('Update')
      }else{
        setColor('');
        setFabric('');
        setStyle('');
        setPictureURL('');
        setLocation('');
        setUpdateOrCreateString('Create')
      }
      
    }, [editFormBoolean])


    async function fetchHat(){
      console.log('fetchHat is being called')
      if (editFormBoolean){
      const response = await fetch(`http://localhost:8090/api/hats/${hatId}/`);
      console.log(response)
      if(response.ok){
        const data = await response.json()
        const hat = data.hat
        console.log(data)
        setColor(hat.color)
        setFabric(hat.fabric)
        setStyle(hat.style)
        setPictureURL(hat.picture_url)
        setLocation(location)
      }
    }

    }
    
    async function handleSubmit(event) {
      event.preventDefault();
      
      const data ={
        color,
        fabric,
        style,
        picture_url,
        location,
      };

      const hatURL = editFormBoolean ? `http://localhost:8090/api/hats/${hatId}/` : 'http://localhost:8090/api/hats/';
      const method = editFormBoolean ? 'PUT' : 'POST'
      const fetchOptions = {
        method,
        body: JSON.stringify(data),
        headers:{
          'Content-Type': 'application/json',
        },
      };

      const hatResponse = await fetch(hatURL, fetchOptions);
      console.log('HAT RESPONSE === ', hatResponse)

      
      
      if (hatResponse.ok) {
        setColor('');
        setFabric('');
        setStyle('');
        setPictureURL('');
        setLocation('');
        getHats();
        setEditFormBoolean(false)
      }else {
        console.error(`An error occurred ${errorMessageString} the hat`);
      }
      
    }
    
    const errorMessageString = editFormBoolean === true ? 'updating' : 'creating'


  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>{updateOrCreateString} a new hat</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input value={color} onChange={(e) => setColor(e.target.value)} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input value={fabric} onChange={(e) => setFabric(e.target.value)} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input value={style} onChange={(e) => setStyle(e.target.value)} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
              <label htmlFor="style">Style</label>
            </div>
            <div className="form-floating mb-3">
              <input value={picture_url} onChange={(e) => setPictureURL(e.target.value)} placeholder="Picture URL" required type="url" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="mb-3">
            <select value={location} onChange={(e) => setLocation(e.target.value)} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.href}>{location.closet_name}</option>
                    )
                  })}
              </select>
            </div>
            <button className="btn btn-primary">{updateOrCreateString}</button>
          </form>
        </div>
      </div>
    </div>
  );
}


export default HatForm;

// PUT request
  // create link to form on hatlist -- prop drill?
  // create put request function: method, put, 
    // pull data from selected instance of hat --> hat.id pass 
    // create edit element + element callback
    // make it look like a button


    // in handle submit -> check if its an edit view
    // in use effect check on mount fetches shoe IF it's an edit view
    // pass variables you get from the getHat from detail view of hat




// async function handleSubmit(event) {
  //     event.preventDefault();
  //     const data = {
    //       name,
    //       starts,
    //       ends,
    //       description,
//       location,
//       max_presentations: maxPresentations,
//       max_attendees: maxAttendees,
//     };

//     const locationUrl = 'http://localhost:8000/api/conferences/';
//     const fetchConfig = {
//       method: "post",
//       body: JSON.stringify(data),
//       headers: {
//         'Content-Type': 'application/json',
//       },
//     };
//     const response = await fetch(locationUrl, fetchConfig);
//     if (response.ok) {
//       const newConference = await response.json();
//       console.log(newConference);
//       setName('');
//       setStarts('');
//       setEnds('');
//       setDescription('');
//       setMaxPresentations('');
//       setMaxAttendees('');
//       setLocation('');
//       getConferences()
//     }
//   }

//   function handleChangeName(event) {
//     const { value } = event.target;
//     setName(value);
//   }

//   function handleChangeStarts(event) {
//     const { value } = event.target;
//     setStarts(value);
//   }

//   function handleChangeEnds(event) {
//     const { value } = event.target;
//     setEnds(value);
//   }

//   function handleChangeDescription(event) {
//     const { value } = event.target;
//     setDescription(value);
//   }

//   function handleChangeMaxPresentations(event) {
//     const { value } = event.target;
//     setMaxPresentations(value);
//   }

//   function handleChangeMaxAttendees(event) {
//     const { value } = event.target;
//     setMaxAttendees(value);
//   }

//   function handleChangeLocation(event) {
//     const { value } = event.target;
//     setLocation(value);
//   }