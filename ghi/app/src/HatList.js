import React, { useEffect, useState  } from 'react';
import { NavLink, useParams, Link } from 'react-router-dom';

function HatList(props) {
    
    async function deleteHat(hatId) {
        const response = await fetch(`http://localhost:8090/api/hats/${hatId}`, 
        {
          method: 'DELETE',
        });
    
        if (response.ok) {
          props.getHats(); 
        } else {
          console.error('Failed to delete hat');
        }
      }

      return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Style</th>
              <th>ID</th>
              <th>Picture Link</th>
              <th>Fabric</th>
              <th>Color</th>
              <th>Delete</th>
              <th>Edit</th>
            </tr>
          </thead>
          <tbody>
            {props.hats.map(hat => {
              return (
                <tr key={hat.id}>
                  <td>{ hat.style }</td>
                  <td>{ hat.id }</td>
                  <td>{ hat.picture_url }</td>
                  <td>{ hat.fabric }</td>
                  <td>{ hat.color }</td>

                  <td>
                    <button onClick={() => deleteHat(hat.id)}>Delete</button>
                  </td>
                  <td>
                    <Link to={`edit/${hat.id}`}> Edit </Link>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      );
        }
export default HatList;






// function HatList() {
//     const [hats, setHats] = useState([]);

//     useEffect(() => {
//         fetch('http://localhost:8090/api/hats/')
//             .then(response => response.json())
//             .then(data => setHats(data.hats));  // make sure to update 'data.hats' according to your API response structure
//     }, []);

//     return (
//       <table className="table table-striped">
//         <thead>
//           <tr>
//             <th>Name</th>
//             <th>Picture</th>
//           </tr>
//         </thead>
//         <tbody>
//           {hats.map(hat => (
//             <tr key={hat.href}>
//               <td>{hat.name}</td>
//               {/* <td>{hat.picture_url}</td> */}
//             </tr>
//           ))}
//         </tbody>
//       </table>
//     );
// }
  
// export default HatList;






















// function HatList(props) {
//     console.log(props.attendees)
//     return (
//       <table className="table table-striped">
//         <thead>
//           <tr>
//             <th>Name</th>
//             <th>Picture</th>
//           </tr>
//         </thead>
//         <tbody>
//         {props.hats.map(hat => {
//             return (
//               <tr key={hat.href}>
      
//                 <td>{ hat.name }</td>
//                 <td>{ hat.picture_url }</td>
//               </tr>
//             );
//           })}
//         </tbody>
//       </table>
//     );
//   }
  
//   export default HatList;