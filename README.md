# Wardrobify

Team:

* Baiyu Hua - Shoes
* Nick de la Flor - Hats

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice
Explain your models and integration with the wardrobe
microservice here.
The hat's microservice is a RESTful API that contains two models.

    1. Hat: 
        - fabric
        - style
        - color
        - picture_url
        The Hat model is the entity object in the context of this application. Hat contains specific attributes used to describe the different attributes but each instance
    of the hat is unique based of its ID (or PK).

    Hat also maintains a Foreign key relationship, where many instances of the hat model can be at one location. 
        2. LocationVO (value object):
        - import href
        - closet name
        - shelf number
        The LocationVO model is a value object because regardless of the ID, if two locations have the same property, that would make them the effectively the same object.
        
    In the database, the import_href property is what enforces uniqueness, however Bob's Hat Shop will always be the same regardless of the import href.

    The Hat app polls the wardrobe monolith for location data.
        Within the wardrobe monolith contains the actual Location entity. The entity is polled through the use of a poller that resides in the hats API app. The hats app polls http://wardrobe-api:8000/api/locations/ for updates every 60 seconds. When a Location entity is created the the poller will update the LocationVO model database for reference information. For this project, only the necessary information was included and serialized. This was the import_href as well as the closet_name.
 Users are able to create, view and delete hats on the frontend.
