import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

from hats_rest.models import LocationVO
# Import models from hats_rest, here.
# from hats_rest.models import Something

def poll_locations():
    print('Got here! -- Your poller working')
    response = requests.get("http://wardrobe-api:8000/api/locations/")
    content = json.loads(response.content)
    for location in content["locations"]:
            LocationVO.objects.update_or_create(
            import_href=location["href"], #enforces uniqueness
            defaults={"closet_name": location["closet_name"]},
              # this HAS to correspond with the the NON null values on the VO
                #based on questions I was asking Josh
            )
             # the properties of the VO don't have to match the original object
                # there will be properties that exist on the VO
                    #that do not exist in teh original entities in the final project
def poll():
    while True:
        print('Hats poller polling for data')
        try:
            poll_locations()
            pass   
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
