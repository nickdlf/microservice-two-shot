from django.shortcuts import render

from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Hat, LocationVO
# Create your views here.

from common.json import ModelEncoder
from .models import LocationVO, Hat

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "id",
        "import_href",
        "closet_name"

    ]



class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style",
        "color",
        "picture_url",
   
    ]


    def get_extra_data(self, o):
        return {"location": o.location.closet_name}




class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "color",
        "fabric",
        "style",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }
    # def get_extra_data(self, o):
    #     return {"location": o.location}



@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method=="GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse({"hats": hats}, encoder=HatListEncoder)
    else:
        content = json.loads(request.body)
        print(content)
        try:
            location = LocationVO.objects.get(import_href=content["location"])
            content["location"] = location

        except LocationVO.DoesNotExist:
            return JsonResponse({"message": "Invalid location Id"})
        hat = Hat.objects.create(**content)

        return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)



    


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_hat(request, pk):
    #####----------GET-------------##########
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        # weather = get_weather_data(
        #     conference.location.city,
        #     conference.location.state.abbreviation,
        # )
        return JsonResponse(
            {"hat": hat},      # "weather": weather},
            encoder=HatDetailEncoder,
            safe=False,
        )
        #####----------DELETE-------------##########
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    
        #####----------PUT-------------##########
    else:     

        content = json.loads(request.body)
        print(content)
        try:

            if "location" in content:
                # convert location href to location object
                # part of the Django ORM
                # so that Database can form association and relation between the two
                location = LocationVO.objects.get(import_href=content["location"])
                content["location"] = location
                #location: <LocationVO Object>
                    # since it's a FOREIGN key object it expects 
                    # location to be an object 
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location_vo id"},
                status=400,
            )

        Hat.objects.filter(id=pk).update(**content)
        conference = Hat.objects.get(id=pk)
        return JsonResponse(
            conference,
            encoder=HatDetailEncoder,
            safe=False,
        )





# @require_http_methods(["PUT", "DELETE", "GET"])
# def api_show_shoe(request, id):
#     if request.method=="GET":
#         hat = Hat.objects.get(id=id)
#         return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)
#     elif request.method=="PUT":
#         content = json.loads(request.body)
#         print(content)
#         try:
#             if "location" in content:
#                 location = LocationVO.objects.get(import_href=content["location"])
#                 content["location"] = location
#         except LocationVO.DoesNotExist:
#             return JsonResponse( { "message": "Invalid location Id" })

#         Hat.objects.filter(id=id).update(**content)
#         hat = Hat.objects.get(id=id)
#         return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)
#     else: # DELETE
#         count, _ = Hat.objects.filter(id=id).delete()
#         return JsonResponse({"deleted": count > 0})

    






# @require_http_methods(["GET", "POST"])
# def api_list_attendees(request, location_vo_id=None):

#     if request.method == "GET":
#         if location_vo_id is not None:
#             # if we're specifying a conference value --> filter attendees by conference
#             hats = Hat.objects.filter(location=location_vo_id)
#         else:
#             # list all attendees if conferences not specified
#             attendees = Hat.objects.all()
#         return JsonResponse(
#             {"attendees": attendees},
#             encoder=HatListEncoder,
#         )
#     else:
#         content = json.loads(request.body)

#         # Get the Conference object and put it in the content dict
#         try:
#             location_href = content["location"]
#             # fetch location instance that has import_href equal to location_href (the location href we POSTed in)
#             location = LocationVO.objects.get(import_href=location_href)
#             # update value of location int or string w/ content object at key position 'location'
#             #  this will allow for a Django ORM relationship between  
#             content["location"] = location
#         except LocationVO.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid location id"},
#                 status=400,
#             )

#         hat = Hat.objects.create(**content)
#         return JsonResponse(
#             hat,
#             encoder=HatDetailEncoder,
#             safe=False,
#         )
        # content = json.loads(request.body)
        # print(content)
        # try:
        #     location_href = content["location"]
        #     # fetch conference instance that has import_href equal to conference_href (the conference href we POSTed in)
        #     location = LocationVO.objects.get(import_href=location_href)
        #     # update value of conference int or string w/ content object at key position 'conference'
        #     #  this will allow for a Django ORM relationship between  
        #     content["location"] = location
        # except LocationVO.DoesNotExist:
        #     return JsonResponse(
        #         {"message": "Invalid location id"},
        #         status=400,
        #     )
        # hat = Hat.objects.create(**content)
        # return JsonResponse(
        #     hat,
        #     encoder=HatDetailEncoder,
        #     safe=False,
        # )
# def api_show_hat(request, pk):
#     """
#     Returns the details for the Attendee model specified
#     by the pk parameter.

#     This should return a dictionary with email, name,
#     company name, created, and conference properties for
#     the specified Attendee instance.

#     {
#         "email": the attendee's email,
#         "name": the attendee's name,
#         "company_name": the attendee's company's name,
#         "created": the date/time when the record was created,
#         "conference": {
#             "name": the name of the conference,
#             "href": the URL to the conference,
#         }
#     }
#     """
#     hat = Hat.objects.get(id=pk)
#     return JsonResponse(
#         hat,
#         encoder=HatDetailEncoder,
#         safe=False,
#     )





# @require_http_methods(["GET", "POST"])
# def api_list_hats(request, location_vo_id=None):
#     if request.method == "GET":
#         if location_vo_id is not None:
#             # if we're specifying a conference value --> filter attendees by conference
#             hat = Hat.objects.filter(location=location_vo_id)
#         else:
#             # list all attendees if conferences not specified
#             hat = Hat.objects.all()
#             return JsonResponse(
#                 {"hats": hat},
#                 encoder=HatListEncoder,
#                 )
#     else:
#         content = json.loads(request.body)

#         # Get the location object and put it in the content dict from request body
#         try:
#             location_href = content["location"]
#             # fetch conference instance that has import_href equal to conference_href (the conference href we POSTed in)
#             location = LocationVO.objects.get(import_href=location_href)
#             # update value of conference int or string w/ content object at key position 'conference'
#             #  this will allow for a Django ORM relationship between  
#             content["location"] = location
#         except LocationVO.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid location id"},
#                 status=400,
#             )

#         hat = Hat.objects.create(**content)
#         return JsonResponse(
#             hat,
#             encoder=HatDetailEncoder,
#             safe=False,
#         )
