from django.db import models



class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100, null = True)
    section_number = models.PositiveSmallIntegerField(null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)
# Create your models here.
class Hat(models.Model):
    fabric = models.CharField(max_length=255, null=True)
    style = models.CharField(max_length=255, null=True)
    color = models.CharField(max_length=255, null=True)
    picture_url = models.URLField(max_length=255, null=True)
    
    location = models.ForeignKey(
        LocationVO, 
        related_name="hats",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name


